Sevendie

INTRODUCTION
------------

Sevendie is an admin theme based on the Drupal core theme, Seven
(https://www.drupal.org/project/seven). Has a lot of visual improvements
compared to the Seven theme.

Very often have to work in the administrative interface, and this theme
allows you to do it with great comfort. Besides, it's nicer on the eyes than
the standard theme Seven. The theme should appeal to fans of the standard
administrative theme, but this theme is a little boring. ;)

 * In the theme used standard fonts installed by default for different operating
systems.
 * Added highlighting rows in tables with a mouse hover.
 * Rounded corners of many of the UI elements and and other visual improvements.

REQUIREMENTS
------------

This theme requires the following parent theme:

 * Seven (https://www.drupal.org/project/seven)

INSTALLATION
------------

How to use it?

1. Go to admin/appearance/
2. Enable Sevendie
3. Under ADMINISTRATION THEME at the bottom of the page, select Sevendie from
the dropdown list & save

CONFIGURATION
-------------

 * Configure theme settings in Administration » Settings » Sevendie
(/admin/appearance/settings/sevendie)
